<?php
$slides = getSlides();
if($slides >0){
?>
<section id="main-banner">
    <div class="swiper-container">
        <div class="swiper-wrapper">
            <?php for ($i=0;$i< count($slides);++$i){ ?>
            <div class="swiper-slide" style="background:url('uploads/slides/<?= $slides[$i]['image']?>') no-repeat 0 0 transparent;background-size:100% 100%;">
                <div class="container">

                    <div class="block1">
                        <h3><?= $slides[$i]['title']?></h3>
                    </div>
                </div>
            </div>
            <?php } ?>
        </div>
        <!-- Add Pagination -->
        <div class="swiper-pagination"></div>

        <!-- Add Arrows -->
        <div class="swiper-button-next"><i class="fa fa-long-arrow-right" aria-hidden="true"></i></div>
        <div class="swiper-button-prev"><i class="fa fa-long-arrow-left" aria-hidden="true"></i></div>
    </div>
</section>
<?php
}
?>