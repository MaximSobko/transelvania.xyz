<section id="counters">
    <div class="container">
        <div class="content">
            <div >
                <img src="../../img/piple.png" alt="">
                <h5>GUESTS</h5>
                <p><?= count(getID()) ?></p>
                <div class="lines"></div>
            </div>
            <div>
                <img src="../../img/coffe.png" alt="">
                <h5>COMMENTS</h5>
                <p>207</p>
                <div class="lines"></div>
            </div>
            <div>
                <img src="../../img/talk.png" alt="">
                <h5>BLOG POSTS</h5>
                <p><?=count(getNews())?></p>
                <div class="lines"></div>
            </div>
            <div>
                <img src="../../img/like.png" alt="">
                <h5>LIKES</h5>
                <p>873</p>
                <div class="lines"></div>
            </div>
            <div>
                <img src="../../img/launch.png" alt="">
                <h5>WE LAUNCHED</h5>
                <p>12 004</p>
                <div class="lines"></div>
            </div>
        </div>
    </div>
</section>