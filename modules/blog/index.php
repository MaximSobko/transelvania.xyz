<section id="blog">
    <div class="container">
        <div class="h2">
            <h1>my blog #maxim</h1>
            <h4>blog for portfolio</h4>
        </div>
        <?php
        $news = getNews();
        if(isset($news) > 0  ){
            for($i=0;$i<count($news);++$i){  ?>


                <article class="blog">
                    <div class="date">
                        <time ><?= $news[$i]['created']?></time>
                    </div>
                    <div  >
                        <h2>
                            <a href="index.php?page=modules/blog/news&id=<?=$news[$i]['id']?>"><?=$news[$i]['title']?></a>
                        </h2>
                    </div>
                    <img src="<?= '/uploads/news/'.$news[$i]['image']?>">
                    <div class="snippet">
                        <p>
                            <?=$news[$i]['text']?>
                        </p>
                        <p class="continue">
                            <a href="index.php?page=modules/blog/news&id=<?=$news[$i]['id']?>">Continue reading...</a>
                        </p>
                    </div>
                    <footer class="meta">
                        <p>
                            Comments:
                            <a href="/16/a-day-with-symfony2#comments"><?= count(getAllCommetsById($news[$i]['id']))?></a>
                        </p>
                        <p>
                            Posted by
                            <span class="highlight"><?=$news[$i]['autor']?></span>
                            at <?=$news[$i]['time'].'PM'?>
                        </p>

                    </footer>
                </article>
        <?php
            }
        }
        ?>
    </div>
</section>