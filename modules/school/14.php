<p>
  Д/з :<br>  Сделать калькулятор, принимающий данные из формы и вывести их на экране
</p>

<?php
//is_int(); Проверка переменной на число ;
//is_array(); (проверка переменной на массив) возвращает тру фолс
if (!empty($_POST['calculator'])) {
    $calculator = $_POST['calculator'];
    $resault = explode(' ', $calculator, 3);

    if(is_array($resault) && (count($resault) == 3 ) ){
        foreach ($resault as $k => $v) {
            if ($k == 0) {
                $num1 = $v;
            } elseif ($k == 1) {
                $action = $v;
            } elseif ($k == 2) {
                $num2 = $v;
            }
        }
        $resault = setSumm($num1,$num2,$action);
    }elseif(is_array($resault) && (count($resault) == 2)){
        foreach ($resault as $k => $v) {
            if ($k == 0) {
                $num1 = $v;
            } elseif ($k == 1) {
                $num2 = $v;
            }
        }
        $resault = setSumm($num1,$num2);
    }else{
        $resault = "Смотри пример выше";
    }
}else {
    $resault = 'НЕЧЕГО НЕ ВВИЛИ';
}
function setSumm($x,$y,$z ='+'){
    switch ($z){
        case '+':
            $resault = $x + $y;
            break;
        case '-':
            $resault = $x - $y;
            break;
        case '*':
            $resault = $x * $y;
            break;
        case '/':
            $resault = $x / $y;
            break;
        case '%':
            $resault = $x % $y;
            break;
        default :
            $resault = '<i>Между цыфрами и оператором должны быть пробелы (10 + 10 )</i>';
    }
    return $resault;
}
?>
<h1> Калькулятор </h1>
<form action="" method="post">
    <p> Пример для ввода данных  данные вводятся через пробел  <i><b>x</b></i> + <i><b>y</b></i> </p>
    <p>Калькулятор
        <input autofocus type="text" placeholder="" name="calculator">
        <?php if(isset($resault)){echo " = <i>".$resault."</i>";} ?>
    </p>
    <input type="submit" value="посчитать">
</form>
