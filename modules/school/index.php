<?php
if (!isset($_SESSION['login']) ){
    $url = "http://".$_SERVER['SERVER_NAME']."/index.php";
    header('Location: ' . $url);
    exit();
}

?>
<section class="menu_lesson">
    <div class="container">
        <a href="?page=modules/school/index&lesson=11">lesson-11</a> <span>|</span>
        <a href="?page=modules/school/index&lesson=12">lesson-12</a> <span>|</span>
        <a href="?page=modules/school/index&lesson=13">lesson-13</a> <span>|</span>
        <a href="?page=modules/school/index&lesson=14">lesson-14</a> <span>|</span>
        <a href="?page=modules/school/index&lesson=15">lesson-15</a> <span>|</span>
        <a href="?page=modules/school/index&lesson=16">lesson-16</a> <span>|</span>
        <a href="?page=modules/school/index&lesson=17">lesson-17</a> <span>|</span>
        <a href="?page=modules/school/index&lesson=18">lesson-18</a> <span>|</span>
        <a href="?page=modules/school/index&lesson=19">lesson-19</a> <span>|</span>
        <a href="?page=modules/school/index&lesson=userip">User-Ip</a> <span>|</span>
    </div>
</section>
<section class="practics">
    <div class="container">
        <?php
            if(!isset($lessons)){$lessons = 'default';}
            include_once $lessons.".php";
        ?>
    </div>

</section>

