<?php
error_reporting(-1);
session_start();

include_once 'libs/mysql.php';
include_once 'libs/my_fu.php';
include_once 'config/index.php';
include_once 'config/head.php';
?>
<body>
<header>
	<div class="container">
		<div class="content">
			<div class="logo">
				<a href="/">
					<!--			<img src="img/logo.png" alt="logotip">-->
					<h1>WoMax</h1>
				</a>
			</div>
			<div class="menu">
				<div ><a <?=@getActive($_GET['page'],'modules/home');?> href="?page=modules/home"><p>Home</p></a></div>
				<div><a <?=@getActive($_GET['page'],'modules/abouts/index');?> href="?page=modules/abouts/index"><p>About Us</p></a></div>
			<!--<div><a href="#"><p>Portfolio</p></a></div>-->
				<div><a  <?=@getActive($_GET['page'],'modules/services/index'); ?> href="?page=modules/services/index"> <p>Features</p></a></div>
				<div><a <?=@getActive($_GET['page'],'modules/blog/index'); ?> href="?page=modules/blog/index"><p>Blog</p></a></div>
			<!--<div><a href="#"><p>Pricing</p></a></div>-->
				<?php if(isset($_SESSION['login']) == 'admin'){?>
						<div><a <?=@getActive($_GET['page'],'modules/school/index'); ?> href="?page=modules/school/index"><p>php</p></a></div>
				<?php } ?>

				<!--<div><a href="#"><p>Contacts</p></a></div>-->
			</div>
			<div class="cart">

			</div>
			<div class="search">

			</div>
		</div>
		<div class="auth">
			<?php
				if(isset($_SESSION['login'])){
					echo "<div id=\"authorization\">  <a href='?page=modules/admin/index'>".$_SESSION['login']. "</a> <a href=\"?page=modules/auth/exit\"> Logout</a></div>";
				}else{
                    echo "<div id=\"authorization\"><a href=\"?page=modules/auth/index\"> Log in</a></div>";
                }
			?>
        </div>
	</div>
</header>
<div id="main">
	<?php include_once $page.'.php'; ?>
</div>
<footer>
	<?php include_once 'config/footer.php';?>
</footer>
	<script src="js/jquery-3.1.1.min.js"></script>
	<script src="js/swiper/swiper.min.js"></script>
	<script src="js/lightbox/lightbox.js"></script>
	<script src="js/notify.min.js"></script>
	<script src="js/script.js"></script>
</body>
</html>
