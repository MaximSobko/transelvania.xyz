<section id="top">
    <div class="container">
        <div class="content">
            <div class="partner"><img src="../img/f_logo.png" alt=""></div>
            <div class="partner"><img src="../img/f_logo1.png" alt=""></div>
            <div class="partner"><img src="../img/f_logo3.png" alt=""></div>
            <div class="partner"><img src="../img/f_logo4.png" alt=""></div>
        </div>
    </div>
</section>
<section id="copyright">
    <div id="back-top">
        <button class="btn"><i class="fa fa-long-arrow-up" aria-hidden="true"></i></button>
    </div>
    <div class="logo">
        <a href="../index.php">
            <img src="../img/logo.png" alt="logotip">
            <h1>WoMax</h1>
        </a>
    </div>
    <div class="container">
        <div class="content">
            <div class="cop">
                <p>Copyright © <?php echo $years?> <a style="color: #f309f5;" href="#">WoMax</a> |   FAQ   |   Suport</p>
            </div>
        </div>
    </div>
</section>
