<?php
include_once 'index.php';
include_once 'headers.php';

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title> WoMax</title>
    <link rel="stylesheet" href="../style/style.css">
    <link href="https://fonts.googleapis.com/css?family=Poiret+One" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Trirong" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Homenaje|Josefin+Slab|Orbitron" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Nixie+One" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Cinzel|Cutive+Mono|Frijole|Josefin+Slab|Julius+Sans+One|Miss+Fajardose|Montez|Nixie+One|Rock+Salt|Six+Caps|Special+Elite|Syncopate" rel="stylesheet">

    <link href="../js/lightbox/lightbox.css" rel="stylesheet">
    <link href="../style/font-awesome.min.css" rel="stylesheet">
    <link href="../style/swiper.min.css" rel="stylesheet" >
    <link href="../style/style.css" rel="stylesheet">
    <link rel="stylesheet" href="../style/jcarousel.responsive.css">
</head>
