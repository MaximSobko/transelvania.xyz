$(function(){
			$('body').on('click','#back-top',function(e){
				 $('html, body').animate({ scrollTop: 0}, 500);
			});

			var swiper = new Swiper('#main-banner .swiper-container', {
        pagination: '.swiper-pagination',
        paginationClickable: true,
         nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
        loop:true,
        autoplay: 2000
    });
      $('.add-to-cart').click(function(){
            var quantityObj = $(this).closest('.single-product').find('.product-quanty');
            var quantity = quantityObj.length ? quantityObj.val() : 1;
            $.post( "/cart/add/"+$(this).data('product-id'),{quantity: quantity}, function( response ) {
              if(response && response.success == true){
                var counter = $('#global_cart_counter');
                var nextVal = parseInt(counter.data('value')) + parseInt(quantity);
                counter.text(nextVal).data('value', nextVal);
                $.notify(response.message, "success");
              }
              else{
                $.notify(response.message, "error");
              }
            });
      });
      $('.btn-addtowishlist').click(function(){
        var self = $(this);
        var product_id = $(this).closest('.single-product').data('product-id');
        self.toggleClass('active');
        $.post( "/favourite/"+product_id+"/modify", function( response ) {
          if(!response.success) self.toggleClass('active');
        });
      });
      $('#product-quanty').change(function(){
        $('#total_product-price').text('$ '+$(this).val()*$(this).data('price'));
      });
    /***

    **/
    /***********CHECKOUT***************/
    /*************quantity feature*******************/
    var checkout_product = $('.checkout-product');
    var checkout_quantity = checkout_product.find('.checkout-quantity');
    var quantity_input = checkout_quantity.find('input');
    quantity_input.change(function(){
      cartQuantityChange($(this));
    });
    checkout_product.find('.checkout-delete').click(function(){
      var cart = $(this).closest('.checkout-product');
      var cartId = cart.data('cart-id');
      $.post( "/cart/"+cartId+"/delete", function(response){
        cart.remove();
        CartChanged(response);
      });
    });

    checkout_quantity.find('.checkout-quantity-modify').click(function(){
      var current_input = $(this).closest('.checkout-quantity').find('input');
      current_input.val(parseInt(current_input.val())+parseInt($(this).data('modify')));
      cartQuantityChange(current_input);
    });

    var cartQuantityChange = function(input){
      var cart = input.closest('.checkout-product');
      var cartId = cart.data('cart-id');
      var re = /^[1-9][0-9]?$|^100$/;
      var quantity = input.val();
      var test = quantity.match(re);
      if(!test){
        input.val(1);
      }
      quantity = input.val();

      var price = parseInt(cart.find('.product-price').data('price'));
      cart.find('.product-total-price').text('$ '+quantity*price);

      $.post( "/cart/"+cartId+"/edit",{quantity: quantity}, CartChanged);
    };
    var CartChanged = function(response){
          if(response && response.success == true){
          var counter = $('#global_cart_counter');
          var nextVal = response.count;
            if(nextVal == 0){
              window.location.reload();
            }
            counter.text(nextVal).data('value', nextVal);
            $('#all-total').text('$ ' + getTotalPrice());
            $.notify(response.message, "success");
          }
          else{
            $.notify(response.message, "error");
          }
    }
    var getTotalPrice = function(){
      var total = 0;
      $('#cart-list').find('.checkout-product').each(function(){
        var quantity = parseInt($(this).find('.product-quantity input').val());
        var price = parseInt($(this).find('.product-price').data('price'));
        total += quantity*price;
      })
      return total;
    }
});


$(window).scroll(function() {
if ($(this).scrollTop() > 100){
  $('#back-top').fadeIn(500);

}
else {
	$('#back-top').fadeOut(500);
}

});

